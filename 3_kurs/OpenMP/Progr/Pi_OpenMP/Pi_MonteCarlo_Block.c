// C Compiler flag:  -fopenmp
/*
Расчет числа пи методом Монте-Карло
Распараллеливание через блоки
*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>


#define N 5000000 // число испытаний в каждом потоке
#define NumThr 4

int InsideCircle(int n)
{ // считаем сколько точек попадают случайным образом внутри окружности
  int inside=0;
  double x,y;
  for (int i = 0; i < n; i++){
    // Сгенерируем случайное число от -1 до 1:
    x = 2*(((double) rand() / ( RAND_MAX))-0.5);
    y = 2*(((double) rand() / ( RAND_MAX))-0.5);
  //  printf("x %f\n",x);
    if ((x*x+y*y)<1){
      inside=inside+1;
    }

  }
  int myid = omp_get_thread_num();
  printf("Hello, I'm thread number %d\n", myid);
  printf("points inside:  %d\n", inside);
return inside;
}


int main(int argc, char *argv[])
{
  double Pi;
  int Glob_N_ins=0;

  omp_set_dynamic(0);      // запретить библиотеке openmp менять число потоков во время исполнения
  omp_set_num_threads(NumThr); // установить число потоков
 double start = omp_get_wtime( );



#pragma omp parallel
  {
    int N_inside,myid;

    N_inside=InsideCircle(N);
    Glob_N_ins=Glob_N_ins+N_inside;

  }


Pi= 4.0*((double) Glob_N_ins )/ (NumThr*N);

   printf ("RAND_MAX = %d\n", RAND_MAX);
   printf ("Number of points inside circle = %d\n", Glob_N_ins);
   printf ("Pi = %f\n", Pi);
   double end = omp_get_wtime( );
   printf("Execution time in seconds = %f\n", end-start);
}
