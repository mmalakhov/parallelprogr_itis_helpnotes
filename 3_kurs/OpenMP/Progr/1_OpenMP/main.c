// C Compiler flag:  -fopenmp
#include <stdio.h>
#include <omp.h>

#define N 20

int main(int argc, char *argv[])
{
  double a[N], b[N], c[N];
  int i, myid;
  omp_set_dynamic(0);      // запретить библиотеке openmp менять число потоков во время исполнения
  omp_set_num_threads(4); // установить число потоков в 2

  // инициализируем массивы
  for (i = 0; i < N; i++)
  {
      a[i] = i * 1.0;
      b[i] = i * 2.0;
  }

  // вычисляем сумму массивов
#pragma omp parallel for shared(a, b, c) private(i, myid)
   for (i = 0; i < N; i++){
     c[i] = a[i] + b[i];
     myid = omp_get_thread_num();
     printf("Hello, I'm thread number %d\n", myid);
    // printf(" and my value c[i] is %d\n",c[i]); //не работает shared, в то время как private можно посмотреть
    }

  printf ("%f\n", c[10]);
  return 0;
}
