// C Compiler flag:  -fopenmp
#include <stdio.h>
#include <omp.h>


#define N 2000

int main(int argc, char *argv[])
{
  double a[N], b[N],a1[N], b1[N], c[N], a2;
  int i, k,l, myid;

  omp_set_dynamic(0);      // запретить библиотеке openmp менять число потоков во время исполнения
  omp_set_num_threads(4); // установить число потоков в 4
 double start = omp_get_wtime( );
  // инициализируем массивы
  for (i = 0; i < N; i++)
  {
      a[i] = i * 1.0;
      a1[i]=  a[i];
      b[i] = i * 2.0;
      b1[i]=b[i];
  }

  // вычисляем сумму массивов
#pragma omp parallel for  shared( c) private(i,k,l, myid, a2) firstprivate(a1,b1)
   for (i = 0; i < N; i++){
     for (k = 0; k < N; k++){
       for (l = 0; l < N; l++){
         a2=b1[l]+a1[k]; // бессмысленные арифметические действия, чтобы занять компьютер
       }

     }
     c[i] = a2 + b1[i];

     myid = omp_get_thread_num();
     printf("Hello, I'm thread number %d\n", myid);
  //   printf(" and my value c[i] is %d\n",c[i]); не работает shared, в то время как private можно посмотреть
    }

   printf ("%f\n", c[10]);
   double end = omp_get_wtime( );
   printf("Execution time in seconds = %f\n", end-start);
}
